﻿using System;

namespace Task2DrawSquare
{
	class Program
	{
		static void Main(string[] args)
		{
			

			bool keepGoing = true;

			while (keepGoing)
			{
				//Menu 
				Console.WriteLine("What would you like to do: " +
					"\n1 - Draw a square " +
					"\n2 - Draw a rectangle" +
					"\n3 - Quit\n");

				int answer = Convert.ToInt32(Console.ReadLine());

				switch (answer)
				{
					case 1:
						drawSquare();
						continue;
					case 2:
						drawRectangle();
						break;
					case 3:
						//Quit
						keepGoing = false;
						break;
					default:
						Console.WriteLine("That's not an option");
						break;
				}
			}
			
		}


		private static void drawSquare()
		{
			Console.WriteLine("What size would you like the square to be: ");
			string sizeString = Console.ReadLine();
			int size = Convert.ToInt32(sizeString);

			for (int i = 0; i < size; i++)
			{
				for (int j = 0; j < size; j++)
				{
					if (i == 0 || i == size - 1 || j == 0 || j == size - 1)
					{
						Console.Write("*");
					}
					else
					{
						Console.Write(" ");
					}
				}
				Console.WriteLine();
			}
			
		}

		private static void drawRectangle()
		{
			Console.WriteLine("Enter height of rectangle: ");
			int width = Convert.ToInt32(Console.ReadLine());

			Console.WriteLine("Enter width of rectangle: ");
			int height = Convert.ToInt32(Console.ReadLine());

			for (int i = 0; i < width; i++)
			{
				for (int j = 0; j < height; j++)
				{
					if (i == 0 || i == width - 1 || j == 0 || j == height - 1)
					{
						Console.Write("*");
					}
					else
					{
						Console.Write(" ");
					}
				}
				Console.WriteLine();
			}
			
			
		}
			
	}
}
